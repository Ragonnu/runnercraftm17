﻿RunnerCraft

Movimiento Personaje:

	A → Desplazamiento Izquierda.
	D → Desplazamiento Derecha.
	Barra Espaciadora → Saltar.
	Click Izquierdo Ratón → Picar.

	W → Picar Bloque superior (solo si se tiene pico de diamante).

Para reiniciar el juego → Click derecho ratón.

	Steve empieza el juego con un pico de madera que solo le permite picar piedra. Cuando consiga 3 de piedra, los gastará y se hará un pico de piedra con el que también podrá picar hierro. Cuando tenga 3 de hierro, los gastará y se hará un pico de hierro. Aquí, a parte de poder picar diamante se le añaden tres plataformas más a la generación de plataformas. Estas pueden ser trampas o parkour.
Por último, cuando consigues el pico de diamante, se te desbloquean 3 plataformas más para la generación de plataformas con un parkour “extremo!” y un Lvl de bonus de diamantito. Más importante! Al tener pico de diamantito puedes picar el bloque de arriba y de enfrente sin tener que saltar obteniendo la habilidad DigHigh que, pulsando W, te permite picar el bloque superior.

Consigue la máxima puntuación y no dejes que Steve desaparezca de la escena!

Bloque Piedra: 10pts
Bloque Hierro: 50pts
Bloque Diamante: 100pts

P.D.: La dificultad está diseñada para niños de 5 a 8 años.

