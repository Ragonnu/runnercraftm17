﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Noob : MonoBehaviour
{
    //velocidad del PJ
    public int speed = 5;
    //Fuerza de salto
    public int force = 150;
    public bool canJump = true;
    public bool gameOver = false;
    public GameObject gameOverText;
    public GameObject pickAxe;
    //Array de escenarios prefabricados
    public GameObject[] blockPrefabs;
    public GameObject floor;
    public AudioSource audioSorce;
    public GameObject score;
    public PickAxe pickAxeScript;
    [SerializeField]
    private static float floorSize = 17.57f;
    //public float offsetXfloor;

    void Start()
    {
        //floorSize = floor.transform.GetChild(0).GetComponent<BoxCollider2D>().size.x;
        //offsetXfloor = floorSize / floor.transform.GetChild(0).childCount / 2;
        //print(transform.name + "  _  " + floorSize);
        //print(floor);

        //asigno el audo de fondo de la escena
        audioSorce = this.GetComponent<AudioSource>();
        //Reproduzco el audio de la escena
        audioSorce.Play();
        pickAxeScript = pickAxe.GetComponent<PickAxe>();
    }

    void Update()
    {
        //Movimiento del pj
        movement();
        //Condiciones de fin de partida
        TheEnd();
    }

    private void TheEnd()
    {
        //Si steve sale de la escena por la izquierda o por abajo, muere.
        if (this.transform.position.x < -9.2f || this.transform.position.y < -6)
        {
            gameOver = true;
            score.SetActive(false);
            gameOverText.SetActive(true);
        }
        //si el juego a terminado y pulsas click derecho del raton, reinicias partida.
        if (gameOver && Input.GetMouseButtonDown(1))
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            gameOver = false;
            score.SetActive(true);
            gameOverText.SetActive(false);
            pickAxeScript.Reset();
            this.transform.position = new Vector2(-4.8f, 0.7f);
            
        }
    }
    //Genera plataformas de manera aleatoria dependiendo del pico que tenga en uso el jugador.
    public void GenerateLvl()
    {
        if (pickAxeScript.diamondPickAxe)
        {
            GameObject newLvl = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length)]);
            newLvl.transform.position = new Vector2(floorSize, 0);
        }
        else if (pickAxeScript.ironPickAxe)
        {
            GameObject newLvl = Instantiate(blockPrefabs[Random.Range(1, 7)]);
            newLvl.transform.position = new Vector2(floorSize, 0);
        }
        else
        {
            GameObject newLvl = Instantiate(blockPrefabs[Random.Range(1, 4)]);
            newLvl.transform.position = new Vector2(floorSize, 0);
        }
        //Debug.Log(floorSize);
    }

    private void movement()
    {
        if (Input.GetKeyDown("space") && canJump)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, force));
            canJump = false;
        }
        if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            //no permite que el jugador salga de la vista de la camara por la parte derecha.
            if (this.transform.position.x >= 7.5)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, this.GetComponent<Rigidbody2D>().velocity.y);
            }
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //si no estoy en el aire puedo saltar
        if (collision.gameObject.tag == "Floor" || collision.gameObject.tag == "Iron" ||
            collision.gameObject.tag == "Diamond" || collision.gameObject.tag == "Stone")
        {
            //Debug.Log("Entro en collision");
            canJump = true;
        }
       
    }
}
