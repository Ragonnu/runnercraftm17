﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyMovement : MonoBehaviour
{
    //Velocidad del cielo
    public float speed = -1f;
    public Noob player;
    
    // Update is called once per frame
    void Update()
    {
        //Si el juego no se ha acavado, el cielo se moverá hacia la izquierda.
        if (!player.gameOver)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0f);
        }
        //Para el movimiento del cielo si el juego ha terminado.
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        }
        //Cuando uno de los gameobjects de cielo sale de la camara por la parte izq. se reposiciona en la parte derecha.
        if (this.transform.position.x <= -19.7)
        {
            this.transform.position = new Vector2(21.22f, 0.3f);
        }
    }
}
