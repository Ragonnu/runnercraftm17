﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SceneMovement : MonoBehaviour
{
    [SerializeField]
    private float sceneMovement = -3.5f;
    public Noob player;
    public PickAxe pickAxe;
    [SerializeField]
    private static float floorSize = 19.2f;
    //public float floorReference;
    //public float offsetXfloor;

    void Start()
    {
        player = FindObjectOfType<Noob>();
        pickAxe = FindObjectOfType<PickAxe>();
        //floorSize = this.transform.GetChild(0).GetComponent<BoxCollider2D>().size.x;
        //offsetXfloor = floorSize / this.transform.GetChild(0).childCount / 2;
        //floorSize = this.transform.GetChild(0).childCount * this.transform.GetChild(0).GetChild(0).localScale.x;
        //floorReference = floorSize + 1.17f;
    }
    void Update()
    {
        //Si el juego no ha terminado, la escena se mueve hacia la izquierda un poco mas
        //rapido que el fondo para dar efecto de profundidad. Si el juego a terminado, se para.
        Movement();
        //si la plataforma sale de la camara y no se ha acabado el juego, la destruirá y llamará
        //a la función GenerateLvl que genera un escenario al azar.
        CallGenerateLvl();
        //increasedifficulty();
    }

    private void CallGenerateLvl()
    {
        if (this.transform.position.x < -floorSize && !player.gameOver)
        {
            //Debug.Log("Entro en if");
            player.GenerateLvl();
            Destroy(this.gameObject);
        }
    }

    private void Movement()
    {
        if (player.gameOver)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(sceneMovement, 0f);
        }
    }

    private void increasedifficulty()
    {
        if (pickAxe.stonePickAxe)
        {
            sceneMovement = -3.5f;
        }
        else if (pickAxe.ironPickAxe)
        {
            sceneMovement = -4.5f;
        }
        else if (pickAxe.diamondPickAxe)
        {
            sceneMovement = -5.5f;
        }
        //else if (Time.time > 20 && Time.time <= 25)
        //{
        //    sceneMovement = -4.5f;
        //}
        //else if (Time.time > 25 && Time.time <= 30)
        //{
        //    sceneMovement = -5f;
        //}
        //else if (Time.time > 30)
        //{
        //    sceneMovement = -6f;
        //}
    }
}
