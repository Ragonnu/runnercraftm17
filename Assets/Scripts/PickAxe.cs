﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickAxe : MonoBehaviour
{
    public bool stonePickAxe = false;
    public bool ironPickAxe = false;
    public bool diamondPickAxe = false;

    public int iron = 0;
    public int stone = 0;
    public int diamond = 0;
    public int score = 0;

    public Canvas canvas;
    public Text ironTxt;
    public Text stoneTxt;
    public Text diamondTxt;
    public Text scoreTxt;
    public Text endScore;

    public Noob player;

    public GameObject woodPA;
    public GameObject stonePA;
    public GameObject ironPA;
    public GameObject diamondPA;
    private AudioSource audiolvl;
    public AudioClip clip;

    // Start is called before the first frame update
    void Start()
    {
        //Asigno el sonido de Upgradear el pico
        audiolvl = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Permite usar la tecla "W" para picar el bloque superior
        DigHigh();
        //Actializa todos los marcadores de la escena
        UpdateText();
        //Comprueba si tienes piedra suficiente para hacer un pico de piedra. Si tiene, lo hace y actualiza bool.
        GetStonePickAxe();
        //Comprueba si tienes hierro suficiente para hacer un pico de hierro. Si tiene, lo hace y actualiza bool.
        GetIronPickAxe();
        //Comprueba si tienes diamante suficiente para hacer un pico de diamante. Si tiene, lo hace y actualiza bool.
        GetDiamondPickAxe();
    }

    private void DigHigh()
    {
        if (Input.GetKey(KeyCode.W) && diamondPickAxe)
        {
            this.transform.position = new Vector2(player.transform.position.x - 0.13f, player.transform.position.y + 0.5f);
        }
        else
        {
            this.transform.position = new Vector2(player.transform.position.x - 0.13f, player.transform.position.y - 0.32f);
        }
    }
    //Pone a cero todos los contadores y desactiva todos los booleanos referente a los picos
    public void Reset()
    {
        diamondPickAxe = false;
        ironPickAxe = false;
        stonePickAxe = false;
        diamondPA.SetActive(false);
        ironPA.SetActive(false);
        stonePA.SetActive(false);
        woodPA.SetActive(true);
        iron = 0;
        stone = 0;
        diamond = 0;
        score = 0;
    }

    private void UpdateText()
    {
        stoneTxt.text = "Stone: " + stone;
        ironTxt.text = "Iron: " + iron;
        diamondTxt.text = "Diamond: " + diamond;
        scoreTxt.text = "Score: " + score;
        endScore.text = "Score: " + score;
    }

    private void GetDiamondPickAxe()
    {
        if (diamond >= 3 && !diamondPickAxe)
        {
            audiolvl.Play();
            diamondPA.SetActive(true);
            ironPA.SetActive(false);
            diamond -= 3;
            diamondPickAxe = true;
        }
    }

    private void GetIronPickAxe()
    {
        if (iron >= 3 && !ironPickAxe && !diamondPickAxe)
        {
            audiolvl.Play();
            stonePA.SetActive(false);
            ironPA.SetActive(true);
            iron -= 3;
            ironPickAxe = true;
        }
    }

    private void GetStonePickAxe()
    {
        if (stone >= 3 && !stonePickAxe && !ironPickAxe && !diamondPickAxe)
        {
            audiolvl.Play();
            stonePA.SetActive(true);
            woodPA.SetActive(false);
            stone -= 3;
            stonePickAxe = true;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        //Lo necesario para picar un bloque de piedra.
        if (collision.gameObject.tag == "Stone" && Input.GetMouseButton(0))
        {
            AudioSource.PlayClipAtPoint(clip, player.transform.position);
            Destroy(collision.gameObject);
            stone++;
            score += 10;
        }
        //Lo necesario para picar un bloque de hierro.
        if (collision.gameObject.tag == "Iron" && Input.GetMouseButton(0) && stonePickAxe)
        {
            AudioSource.PlayClipAtPoint(clip, player.transform.position);
            Destroy(collision.gameObject);
            iron++;
            score += 50;
        }
        //Lo necesario para picar un bloque de diamante.
        if (collision.gameObject.tag == "Diamond" && Input.GetMouseButton(0) && ironPickAxe)
        {
            AudioSource.PlayClipAtPoint(clip, player.transform.position);
            Destroy(collision.gameObject);
            diamond++;
            score += 100;
        }

    }
}
