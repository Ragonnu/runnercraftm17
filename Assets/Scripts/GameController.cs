﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    //public Noob player;
    //public Camera gameCamera;

    public GameObject[] blockPrefabs;
    ////puntero, apunta el final de los bloques generados
    //private float size;
    ////spawn, la variable extra que tiene que "mirar" el puntero para saber si generar un nuevo bloque
    //private float spawn = 17;

    void Start()
    {
        //size = blockPrefabs[0].GetComponent<BoxCollider2D>().size.x;
        //justo al salir creamos un bloque inicial
        //Instantiate(blockPrefabs[0]);
        //Instantiate(blockPrefabs[1], );
    }

    // Update is called once per frame
    void Update()
    {
        //GenerateLvl();

        //hacemos que la camera siga al jugador. La camara usa un vector3
        //gameCamera.transform.position = new Vector3(
        //    player.transform.position.x,
        //    player.transform.position.y,
        //    gameCamera.transform.position.z);

        //if (player != null && this.puntero < player.transform.position.x + spawn)
        //{
            //puntos++;
            //instanciar hace aparecer en la escena activa
           // GameObject newBlock = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length)]);
            //cogemos el tamaño, que seria la escala (tamaño) de x, del primer hijo (el suelo) del bloque prefab
            //float size = newBlock.transform.GetChild(0).transform.localScale.x;
            //float size = 17;
            //ponemos la posicion del tamaño instanciado en la posicion del puntero + la mitad de su tamaño (la posicion que tenemos que indicarle es su centro absoluto)
            //newBlock.transform.position = new Vector2(puntero + size / 2, 0);
            //aumentamos el puntero en el tamaño del bloque
            //puntero += size;
        //}
    }

    public void GenerateLvl()
    {
        GameObject newLvl = Instantiate(blockPrefabs[Random.Range(1, 8)]);
        newLvl.transform.position = new Vector2(17f, 0);
    }
}
